default:
  image: docker:stable

  services:
    - docker:dind

  # Pip's cache doesn't store the python packages https://pip.pypa.io/en/stable/reference/pip_install/#caching
  # If you want to also cache the installed packages, you have to install them in a virtualenv and cache it as well.
  cache:
    # Cache per branch
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .cache/pip
      - venv/

  before_script:
    - apk update && apk fetch openjdk8
    - apk add --update coreutils # To update base64 in order to use option --decode
    - apk add --no-cache curl bash jq python3 py3-pip py3-virtualenv && apk add openjdk8 maven openssl
    - virtualenv venv
    - source venv/bin/activate
    - curl https://sdk.cloud.google.com > install.sh # Download gcloud installer
    - bash install.sh --disable-prompts # Install gcloud
    - export PATH=$PATH:/root/google-cloud-sdk/bin # Add gcloud to PATH
    - echo $PRIVATE_KEY_FILE_CONTENT > /tmp/$CI_PIPELINE_ID.json
    - export CLOUDSDK_CORE_DISABLE_PROMPTS=1 # Equivalent to --quiet if does not work
    - export CLOUDSDK_CORE_PROJECT="$PROJECT_ID"
    - gcloud auth activate-service-account --key-file /tmp/$CI_PIPELINE_ID.json

  after_script:
    - rm /tmp/$CI_PIPELINE_ID.json

variables:
  DOCKER_HOST: tcp://docker:2375/
  NO_PROXY: docker
  DOCKER_DRIVER: overlay2

stages:
  - setup_cluster_deploy
  - unit_test
  - dev_images
  - dev_deploy
  - dev_e2e_test

# Run on all branches except setup-cluster
build:unit-test:
  stage: unit_test
  allow_failure: false
  except:
    - setup-cluster
  script:
    - chmod +x deploy/cicd_unit_test.sh
    - ./deploy/cicd_unit_test.sh

########################################################################################################################
# SETUP CLUSTER (Create branch setup-cluster at the beginning to create k8s cluster)
########################################################################################################################
setup_cluster:deploy:
  stage: setup_cluster_deploy
  allow_failure: false
  only:
    - setup-cluster
  script:
    ## Install terraform & Set GCP credentials to terraform
    - apk add terraform && export GOOGLE_APPLICATION_CREDENTIALS="/tmp/$CI_PIPELINE_ID.json"
    - chmod +x deploy/cicd_setup_cluster_deploy.sh
    - ./deploy/cicd_setup_cluster_deploy.sh

########################################################################################################################
# DEV and MASTER
########################################################################################################################
dev:images:
  variables:
    ENVIRONMENT: dev
    DOCKER_VERSION: $CI_COMMIT_SHA
  stage: dev_images
  allow_failure: false
  only:
    - dev
    - master
  script:
    ## Execute deployment of images
    - chmod +x deploy/cicd_images.sh
    - ./deploy/cicd_images.sh

dev:deploy:
  variables:
    ENVIRONMENT: dev
    DOCKER_VERSION: $CI_COMMIT_SHA
  stage: dev_deploy
  allow_failure: false
  only:
    - dev
    - master
  script:
    ## Execute deployment
    - chmod +x deploy/cicd_deploy.sh
    - ./deploy/cicd_deploy.sh

dev:e2e-test:
  stage: dev_e2e_test
  allow_failure: false
  variables:
    ENVIRONMENT: dev
  only:
    - dev
    - master
  script:
    - chmod +x deploy/cicd_e2e_test.sh
    - ./deploy/cicd_e2e_test.sh
